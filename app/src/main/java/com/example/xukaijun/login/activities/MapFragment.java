package com.example.xukaijun.login.activities;

import android.arch.persistence.room.Room;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xukaijun.login.MainActivity;
import com.example.xukaijun.login.helpers.HttpUrlHelper;
import com.example.xukaijun.login.model.User;
import com.example.xukaijun.login.model.UserDatabase;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.example.xukaijun.login.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class MapFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{
    UserDatabase db = null;
    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private int id;
    String url;
    String data;
    InputStream inputStream;
    BufferedReader bufferedReader;
    StringBuilder stringBuilder;


    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);
        id =  ((MainActivity)getActivity()).getId();
        db = Room.databaseBuilder((MainActivity)getContext(),
                UserDatabase .class, "UserDatabase") .fallbackToDestructiveMigration()
                .build();
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        //LatLng HOME = new LatLng(0,0);

        googleApiClient = new GoogleApiClient.Builder(this.getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
        ReadDatabase readDatabase = new ReadDatabase();
        readDatabase.execute();
    }

    private class ReadDatabase extends AsyncTask<Void, Void, User> {
        @Override
        protected User doInBackground(Void... params) {
            List<User> users = db.userDao().getAll();
            User user = new User();
            for (User temp : users) {
                if (temp.getId() == id){
                    user = temp;
                }
            }
            return user; }
        @Override
        protected void onPostExecute(User user1) {
            laln(user1.getAddress(),user1.getPostcode());
        }
        public void laln(String address,int postcode)
        {
            StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/geocode/json?");
            sb.append("address=" + address +"+" + postcode);
            sb.append("&key=" + getResources().getString(R.string.google_maps_key));
            String url = sb.toString();
            Object dataTransfer[] = new Object[2];
            dataTransfer[0] = mMap;
            dataTransfer[1] = url;
            new GetLatLng().execute(dataTransfer);
        }
        public class GetLatLng extends AsyncTask<Object,String,List<String> > {
            @Override
            protected List<String> doInBackground(Object... objects) {
                mMap = (GoogleMap)objects[0];
                url = (String)objects[1];
                List<User> users = db.userDao().getAll();
                User user = new User();
                for (User temp : users) {
                    if (temp.getId() == id){
                        user = temp;
                    }
                }
                List<String> mix= new ArrayList<>();
                mix.add(new HttpUrlHelper().getHttpUrlConnection(url));
                mix.add(user.getAddress());
                mix.add(String.valueOf(user.getPostcode()));
                return mix;
            }

            @Override
            protected void onPostExecute(List<String> mix) {

                try {
                    JSONObject parentObject = new JSONObject(mix.get(0));
                    JSONArray resultArray = parentObject.getJSONArray("results");
                    String latitude = "";
                    String longitude= "";

                    JSONObject jsonObject = resultArray.getJSONObject(0);
                    JSONObject locationObj = jsonObject.getJSONObject("geometry").getJSONObject("location");
                    latitude = locationObj.getString("lat");
                    longitude = locationObj.getString("lng");
                    park(Double.valueOf(latitude),Double.valueOf(longitude));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }
            public void park (Double latitude,Double longitude){
                LatLng HOME = new LatLng(latitude, longitude);
                MarkerOptions mHome = new MarkerOptions()
                        .position(HOME)
                        .title("Home");
                mMap.addMarker(mHome);
                Circle circle = mMap.addCircle(new CircleOptions()
                        .center(HOME)
                        .radius(5000)
                        .strokeColor(Color.RED));
                mMap.moveCamera(getZoomForDistance(HOME,5000));
                StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
                sb.append("&location=" + latitude + "," + longitude);
                sb.append("&radius=" + 5000);
                sb.append("&types=" + "park");
                sb.append("&key=" + getResources().getString(R.string.google_maps_key));
                String url = sb.toString();
                Object dataTransfer[] = new Object[2];
                dataTransfer[0] = mMap;
                dataTransfer[1] = url;
                new NearByParks().execute(dataTransfer);
            }

            public class NearByParks extends AsyncTask<Object,String,String > {
                @Override
                protected String doInBackground(Object... objects) {
                    mMap = (GoogleMap)objects[0];
                    url = (String)objects[1];
                    return new HttpUrlHelper().getHttpUrlConnection(url);
                }

                @Override
                protected void onPostExecute(String s) {
                    try {
                        JSONObject parentObject = new JSONObject(s);
                        JSONArray resultArray = parentObject.getJSONArray("results");

                        for (int i = 0; i < resultArray.length(); i ++){
                            JSONObject jsonObject = resultArray.getJSONObject(i);
                            JSONObject locationObj = jsonObject.getJSONObject("geometry").getJSONObject("location");
                            String latitude = locationObj.getString("lat");
                            String longitude = locationObj.getString("lng");
                            String parkName = jsonObject.getString("name");
                            String vicinity = jsonObject.getString("vicinity");
                            String icon = jsonObject.getString("icon");
                            LatLng latLng = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));
                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(latLng)
                                    .title(parkName);

//                    URL url = new URL(icon);
//                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//
//                    mMap.addMarker(new MarkerOptions()
//                            .icon(BitmapDescriptorFactory.fromBitmap(bmp)));
                            mMap.addMarker(markerOptions).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }
            }
        }
    }

    private CameraUpdate getZoomForDistance(LatLng originalPosition, double distance){
        LatLng rightBottom = SphericalUtil.computeOffset(originalPosition,distance,135);
        LatLng leftTop = SphericalUtil.computeOffset(originalPosition,distance,-45);
        LatLngBounds sBounds = new LatLngBounds(new LatLng(rightBottom.latitude,leftTop.longitude),new LatLng(leftTop.latitude,rightBottom.longitude));
        return CameraUpdateFactory.newLatLngBounds(sBounds,0);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest().create();
        locationRequest.setInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}

