package com.example.xukaijun.login.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.xukaijun.login.model.Credential;
import com.example.xukaijun.login.model.Cuser;
import com.example.xukaijun.login.model.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "CredentialManager.db";

    // User table name
    private static final String TABLE_CREDENTIAL = "credential";

    // User Table Columns names
    private static final String COLUMN_CREDENTIAL_ID = "credential_username";
    private static final String COLUMN_CREDENTIAL_USID = "credential_usid";
    private static final String COLUMN_CREDENTIAL_PASSWORD = "credential_password";
    private static final String COLUMN_CREDENTIAL_SDATE = "credential_sdate";


    // create table sql query
    private String CREATE_CREDENTIAL_TABLE = "CREATE TABLE " + TABLE_CREDENTIAL + "("
            + COLUMN_CREDENTIAL_ID + " STRING PRIMARY KEY," + COLUMN_CREDENTIAL_USID + " TEXT,"
            + COLUMN_CREDENTIAL_SDATE + " TEXT," + COLUMN_CREDENTIAL_PASSWORD + " TEXT" + ")";

    // drop table sql query
    private String DROP_CREDENTIAL_TABLE = "DROP TABLE IF EXISTS " + TABLE_CREDENTIAL;

    /**
     * Constructor
     *
     * @param context
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CREDENTIAL_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Drop User Table if exist
        db.execSQL(DROP_CREDENTIAL_TABLE);

        // Create tables again
        onCreate(db);

    }

    /**
     * This method is to create user record
     *
     * @param credential
     */
    public void addCredential(Credential credential) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CREDENTIAL_ID, credential.getCusername());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = sdf.format(credential.getSdate());
        values.put(COLUMN_CREDENTIAL_SDATE, dateString);
        values.put(COLUMN_CREDENTIAL_PASSWORD, credential.getCpassword());
        values.put(COLUMN_CREDENTIAL_USID, credential.getUsid().getUsid());
        // Inserting Row
        db.insert(TABLE_CREDENTIAL, null, values);
        db.close();
    }

    public Credential getCredential(String username) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_CREDENTIAL_ID,
                COLUMN_CREDENTIAL_SDATE,
                COLUMN_CREDENTIAL_PASSWORD,
                COLUMN_CREDENTIAL_USID
        };
        // sorting orders
        String sortOrder =
                COLUMN_CREDENTIAL_ID + " ASC";
        List<Credential> credentialList = new ArrayList<Credential>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_CREDENTIAL, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Credential credential =new Credential();
                credential.setCusername(cursor.getString(cursor.getColumnIndex(COLUMN_CREDENTIAL_ID)));
                credential.setCpassword(cursor.getString(cursor.getColumnIndex(COLUMN_CREDENTIAL_PASSWORD)));
                Date date = null;
                try {
                    date = new SimpleDateFormat("yyyy-MM-dd").parse(cursor.getString(cursor.getColumnIndex(COLUMN_CREDENTIAL_SDATE)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                credential.setSdate(date);
                credential.setUsid(new Cuser(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_CREDENTIAL_USID)))));
                // Adding user record to list
                credentialList.add(credential);
            } while (cursor.moveToNext());
        }
        Credential credential = new Credential();
        if (credentialList != null){
        for (Credential credential1:credentialList)
        {
            if (credential1.getCusername().equals(username))
            {
                credential = credential1;
                break;
            }
        }}
        cursor.close();
        db.close();

        // return user list
        return credential;
    }
    }

