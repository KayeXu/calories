package com.example.xukaijun.login.model;

public class Report {

    /**
     * cdate : 2019-03-20T00:00:00+11:00
     * rgoals : 1000
     * rid : 1
     * totalburned : 11232.0
     * totalconsumed : 100.0
     * totalsteps : 1400
     * usid : {"dob":"1995-02-09T00:00:00+11:00","levelofactivity":3,"stepspermile":2200,"uaddress":"Blackburn rd","uemail":"kxuu0012@student.monash.edu","ugender":"M","uheight":180,"uname":"kaye","upostcode":3168,"usid":1,"usurname":"Xu","uweight":100}
     */

    private String cdate;
    private int rgoals;
    private int rid;
    private double totalburned;
    private double totalconsumed;
    private int totalsteps;
    private Cuser usid;

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public int getRgoals() {
        return rgoals;
    }

    public void setRgoals(int rgoals) {
        this.rgoals = rgoals;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public double getTotalburned() {
        return totalburned;
    }

    public void setTotalburned(double totalburned) {
        this.totalburned = totalburned;
    }

    public double getTotalconsumed() {
        return totalconsumed;
    }

    public void setTotalconsumed(double totalconsumed) {
        this.totalconsumed = totalconsumed;
    }

    public int getTotalsteps() {
        return totalsteps;
    }

    public void setTotalsteps(int totalsteps) {
        this.totalsteps = totalsteps;
    }

    public Cuser getUsid() {
        return usid;
    }

    public void setUsid(Cuser usid) {
        this.usid = usid;
    }

}
