package com.example.xukaijun.login.model;

public class Food {
    private Integer fid;
    private String fname;
    private String category;
    private Long camount;
    private String sunit;
    private Integer samount;
    private Long fat;
    public Food() {
    }

    public Food(Integer fid) {
        this.fid = fid;
    }

    public Food(Integer fid, String fname, String category, Long camount, String sunit, Integer samount, Long fat) {
        this.fid = fid;
        this.fname = fname;
        this.category = category;
        this.camount = camount;
        this.sunit=sunit;
        this.samount=samount;
        this.fat=fat;
    }

    public Food(String fname, String category, Long camount, String sunit, Integer samount, Long fat) {
        this.fname = fname;
        this.category = category;
        this.camount = camount;
        this.sunit = sunit;
        this.samount = samount;
        this.fat = fat;
    }

    public Integer getFid() {
        return fid;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    public Long getCamount() {
        return camount;
    }

    public void setCamount(Long camount) {
        this.camount = camount;
    }

    public String getSunit() {
        return sunit;
    }

    public void setSunit(String sunit) {
        this.sunit = sunit;
    }

    public Integer getSamount() {
        return samount;
    }

    public void setSamount(Integer samount) {
        this.samount = samount;
    }

    public Long getFat() {
        return fat;
    }

    public void setFat(Long fat) {
        this.fat = fat;
    }
}
