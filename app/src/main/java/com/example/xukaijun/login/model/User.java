package com.example.xukaijun.login.model;

import java.util.Date;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class User {

    @PrimaryKey public int uid;
    @ColumnInfo(name = "first_name")public String firstname;
    @ColumnInfo(name = "goal_steps")public int goalsteps;
    @ColumnInfo(name = "total_steps")public int totalsteps;
    @ColumnInfo(name = "postcode")public int postcode;
    @ColumnInfo(name = "address")public String address;



    public int getId() {
        return uid;
    }

    public void setId(int id) {
        this.uid = id;
    }

    public String getFirstName() {
        return firstname;
    }

    public void setFirstName(String name) {
        this.firstname = name;
    }
    public int getGoalsteps() {
        return goalsteps;
    }
    public void setGoalsteps(int goalsteps) {
        this.goalsteps = goalsteps;
    }

    public int getTotalsteps() {
        return totalsteps;
    }

    public void setTotalsteps(int totalsteps) {
        this.totalsteps = totalsteps;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}