package com.example.xukaijun.login.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.arch.persistence.room.Room;
import com.example.xukaijun.login.MainActivity;
import com.example.xukaijun.login.R;
import com.example.xukaijun.login.helpers.InputValidation;
import com.example.xukaijun.login.model.Credential;
import com.example.xukaijun.login.model.Cuser;
import com.example.xukaijun.login.model.User;
import com.example.xukaijun.login.model.UserDatabase;
import com.example.xukaijun.login.sql.DatabaseHelper;
import com.example.xukaijun.login.sql.RestClient;

import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    UserDatabase db = null;
    private final AppCompatActivity activity = LoginActivity.this;

    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputLayoutUsername;
    private TextInputLayout textInputLayoutPassword;

    private TextInputEditText textInputEditTextEmail;
    private TextInputEditText textInputEditTextUsername;
    private TextInputEditText textInputEditTextPassword;

    private AppCompatButton appCompatButtonLogin;

    private AppCompatButton appCompatButtonRegister;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private TextInputLayout textInputLayoutName;
    private TextInputLayout textInputLayoutSurname;
    private TextInputLayout textInputLayoutHeight;
    private TextInputLayout textInputLayoutWeight;
    private TextInputLayout textInputLayoutSpm;
    private TextInputLayout textInputLayoutAddress;
    private TextInputLayout textInputLayoutPostcode;
    private TextInputLayout textInputLayoutConfirmPassword;

    private TextInputEditText textInputEditTextName;
    private TextInputEditText textInputEditTextHeight;
    private TextInputEditText textInputEditTextWeight;
    private TextInputEditText textInputEditTextSurname;
    private TextInputEditText textInputEditTextSpm;
    private TextInputEditText textInputEditTextAddress;
    private TextInputEditText textInputEditTextPostcode;
    private TextInputEditText textInputEditTextConfirmPassword;
    private DatePicker simpleDatePicker;
    private Spinner spinner;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;

    private User user;
    private Credential credential;
    private int id = 0;
    private List<Cuser> cuserList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        db = Room.databaseBuilder(getApplicationContext(),
                UserDatabase.class, "UserDatabase") .fallbackToDestructiveMigration()
                .build();
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    /**
     * This method is to initialize views
     */
    private void initViews() {

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        textInputLayoutUsername = (TextInputLayout) findViewById(R.id.textInputLayoutUsername);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);

        textInputEditTextEmail = (TextInputEditText) findViewById(R.id.textInputEditTextEmail);
        textInputEditTextUsername = (TextInputEditText) findViewById(R.id.textInputEditTextUsername);
        textInputEditTextPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);

        appCompatButtonLogin = (AppCompatButton) findViewById(R.id.appCompatButtonLogin);

        appCompatButtonRegister = (AppCompatButton) findViewById(R.id.appCompatButtonRegister);
        textInputLayoutName = (TextInputLayout) findViewById(R.id.textInputLayoutName);
        textInputLayoutSurname = (TextInputLayout) findViewById(R.id.textInputLayoutSurname);
        textInputLayoutHeight = (TextInputLayout) findViewById(R.id.textInputLayoutHeight);
        textInputLayoutWeight = (TextInputLayout) findViewById(R.id.textInputLayoutWeight);
        textInputLayoutSpm = (TextInputLayout) findViewById(R.id.textInputLayoutStepspermile);
        textInputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.textInputLayoutConfirmPassword);
        textInputLayoutAddress = (TextInputLayout) findViewById(R.id.textInputLayoutAddress);
        textInputLayoutPostcode = (TextInputLayout) findViewById(R.id.textInputLayoutPostcode);
        textInputEditTextName = (TextInputEditText) findViewById(R.id.textInputEditTextName);
        textInputEditTextSurname = (TextInputEditText) findViewById(R.id.textInputEditTextSurname);
        textInputEditTextHeight = (TextInputEditText) findViewById(R.id.textInputEditTextHeight);
        textInputEditTextWeight = (TextInputEditText) findViewById(R.id.textInputEditTextWeight);
        textInputEditTextSpm = (TextInputEditText) findViewById(R.id.textInputEditTextStepspermile);
        textInputEditTextAddress = (TextInputEditText) findViewById(R.id.textInputEditTextAddress);
        textInputEditTextPostcode = (TextInputEditText) findViewById(R.id.textInputEditTextPostcode);
        textInputEditTextConfirmPassword = (TextInputEditText) findViewById(R.id.textInputEditTextConfirmPassword);
        spinner = (Spinner) findViewById(R.id.levelofactivity);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.level_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        simpleDatePicker = (DatePicker) findViewById(R.id.DatePickerDOB);
        radioSexGroup = (RadioGroup) findViewById(R.id.radioGroup1);
    }

    /**
     * This method is to initialize listeners
     */
    private void initListeners() {
        appCompatButtonLogin.setOnClickListener(this);
        appCompatButtonRegister.setOnClickListener(this);
    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        databaseHelper = new DatabaseHelper(activity);
        inputValidation = new InputValidation(activity);
        user = new User();
    }

    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.appCompatButtonLogin:
                if(validationlogin()){
                SignAsyncTask signAsyncTask = new SignAsyncTask();
                signAsyncTask.execute(textInputEditTextUsername.getText().toString(), getMD5(textInputEditTextPassword.getText().toString()));
                    try
                    {
                        Thread.sleep(500);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Authenticating...");
                    progressDialog.show();
                    // TODO: Implement your own authentication logic here.

                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();
                                }
                            }, 3000);
                if (signup()){
                Intent mainActivity = new Intent(this, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", id);
                mainActivity.putExtras(bundle);
                startActivity(mainActivity);}}
                break;
            case R.id.appCompatButtonRegister:
                if (validationregister()){
                PostAsyncTask postAsyncTask = new PostAsyncTask();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateString = sdf.format(getDateFromDatePicker(simpleDatePicker));
                String sdate = sdf.format(new Date());
                int selectedId = radioSexGroup.getCheckedRadioButtonId();
                radioSexButton = (RadioButton) findViewById(selectedId);
                postAsyncTask.execute(" ", textInputEditTextName.getText().toString(), textInputEditTextSurname.getText().toString(),
                        textInputEditTextEmail.getText().toString(), dateString, textInputEditTextHeight.getText().toString(), textInputEditTextWeight.getText().toString(),
                        radioSexButton.getText().toString(), textInputEditTextAddress.getText().toString(), textInputEditTextPostcode.getText().toString(),
                        spinner.getSelectedItem().toString(), textInputEditTextSpm.getText().toString(), textInputEditTextUsername.getText().toString(),
                        getMD5(textInputEditTextPassword.getText().toString()), sdate);}
                break;
        }
    }


    private class PostAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Cuser cuser = null;
            Credential credential = null;
            credential = RestClient.credentialfindByUsername(params[12]);
            cuserList = RestClient.findAllCuser();
            boolean flag = true;
            if (credential.getCusername()!=null){
                return "User exist";
            }else {
                for (Cuser cuser1: cuserList)
                {
                    if (cuser1.getUemail().equals(params[3]))
                    {
                        return "Email exist";
                    }}
            }
            try {
                cuser = new Cuser(getNewUserid(), params[1], params[2], params[3], formatter.parse(params[4]), Double.valueOf(params[5]),
                        Double.valueOf(params[6]), params[7], params[8], Short.valueOf(params[9]), Integer.valueOf(params[10]), Long.valueOf(params[11]));
                credential = new Credential(params[12], params[13], formatter.parse(params[14]), cuser);
            } catch (ParseException e) {
                return e.getMessage();
            }
            RestClient.createCuser(cuser);
            RestClient.createCredential(credential);
            return "User was added";
        }

        @Override
        protected void onPostExecute(String response) {
            Snackbar.make(nestedScrollView, response, Snackbar.LENGTH_LONG).show();
        }

        private int getNewUserid() {
            int ID = 0;
            for (Cuser cuser1 : cuserList) {
                if (cuser1.getUsid() > ID) {
                    ID = cuser1.getUsid();
                }
            }
            return ID + 1;
        }
    }

    /**
     * This method is to validate the input text fields and verify login credentials from SQLite
     */
    private boolean validationlogin() {
        if (!inputValidation.isInputEditTextFilled(textInputEditTextUsername, textInputLayoutUsername, "Wrong Username or Password")) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, textInputLayoutPassword, "Wrong Username or Password")) {
            return false;
        }
        return true;
    }

    private boolean validationregister() {
        if (!inputValidation.isInputEditTextFilled(textInputEditTextUsername, textInputLayoutUsername, "Wrong Username or Password")) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, textInputLayoutPassword, "Wrong Username or Password")) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextName, textInputLayoutName, getString(R.string.error_message_name))) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextSurname, textInputLayoutSurname, getString(R.string.error_message_name))) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
            return false;
        }
        if (!inputValidation.isInputEditTextEmail(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
            return false;
        }
        if (!inputValidation.isInputEditTextMatches(textInputEditTextPassword, textInputEditTextConfirmPassword,
                textInputLayoutConfirmPassword, getString(R.string.error_password_match))) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextAddress, textInputLayoutAddress, "Enter Address")) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextHeight, textInputLayoutHeight, "Enter height")) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextWeight, textInputLayoutWeight, "Enter weight")) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextWeight, textInputLayoutWeight, "Enter weight")) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPostcode, textInputLayoutPostcode, "Enter postcode")) {
            return false;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextSpm, textInputLayoutSpm, "Enter step per mile")) {
            return false;
        }
        return true;
    }

    public static String getMD5(String message) {
        MessageDigest messageDigest = null;
        StringBuffer md5StrBuff = new StringBuffer();
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(message.getBytes("UTF-8"));

            byte[] byteArray = messageDigest.digest();
            for (int i = 0; i < byteArray.length; i++) {
                if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                    md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
                else
                    md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
            }
        } catch (Exception e) {
            throw new RuntimeException();
        }
        return md5StrBuff.toString().toUpperCase();
    }

        private class SignAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            Credential credential = RestClient.credentialfindByUsername(params[0]);
            try {
                if (credential != null) {
                    if (params[1].equals(credential.getCpassword())) {
                        postDataToSQLite(credential);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Sign fail: username or password is wrong";
        }

        @Override
        protected void onPostExecute(String response) {
            Snackbar.make(nestedScrollView, response, Snackbar.LENGTH_LONG).show();
            //emptyInputEditText();
        }

        private void postDataToSQLite(Credential credential) {
            User user = new User();
            user.setId(credential.getUsid().getUsid());
            user.setFirstName(credential.getUsid().getUname());
            user.setGoalsteps(0);
            user.setTotalsteps(0);
            user.setAddress(credential.getUsid().getUaddress());
            user.setPostcode(credential.getUsid().getUpostcode());
            db.userDao().insert(user);
            databaseHelper.addCredential(credential);
            // Snack Bar to show success message that record saved successfully
            Snackbar.make(nestedScrollView, getString(R.string.success_message), Snackbar.LENGTH_LONG).show();
            //emptyInputEditText();
        }
    }

    public boolean signup() {
        Credential credential = databaseHelper.getCredential(textInputEditTextUsername.getText().toString());
        if (credential.getCusername() != null ){
            id= credential.getUsid().getUsid();
            return true;}
        else{
            Snackbar.make(nestedScrollView, "username or password is wrong", Snackbar.LENGTH_LONG).show();
            return false;}
    }

    public static java.util.Date getDateFromDatePicker(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }
}
