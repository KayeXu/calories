package com.example.xukaijun.login.activities;

import android.arch.persistence.room.Room;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xukaijun.login.MainActivity;
import com.example.xukaijun.login.R;
import com.example.xukaijun.login.helpers.FatSecretGet;
import com.example.xukaijun.login.helpers.SearchGoogleAPI;
import com.example.xukaijun.login.model.Consumption;
import com.example.xukaijun.login.model.Cuser;
import com.example.xukaijun.login.model.Food;
import com.example.xukaijun.login.model.User;
import com.example.xukaijun.login.model.UserDatabase;
import com.example.xukaijun.login.sql.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DieFragment extends Fragment {
    UserDatabase db = null;
    View dieView;
    private TextView TextView_description,textView_Selected ;
    private AppCompatImageView appCompatImageView_food;
    private List<HashMap<String, String>> categoryArray,foodArray,foodSearchArray;
    private ListView categoryList,foodList,foodSearchList;
    private SimpleAdapter categoryAdapter,foodNameAdapter,foodSearchAdapter;
    private String[] categoryHEAD,foodHEAD,foodSearchHEAD;
    private int[] categoryCELL,foodCELL,foodSearchCELL;
    private Food foodSelected,newFoodSelected;
    private TextInputEditText textInputEditTextSearch,textInputEditTextServe,textInputEditTextCategory;
    private int id;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        db = Room.databaseBuilder((MainActivity)getContext(),
                UserDatabase.class, "UserDatabase") .fallbackToDestructiveMigration()
                .build();
        dieView = inflater.inflate(R.layout.fragment_diet, container, false);
        textInputEditTextSearch = dieView.findViewById(R.id.textInputEditTextSearch);
        textInputEditTextServe = dieView.findViewById(R.id.textInputEditTextServe);
        id =  ((MainActivity)getActivity()).getId();
        textInputEditTextCategory = dieView.findViewById(R.id.textInputEditTextCategory);
        HashMap<String,String> map = new HashMap<String,String>();
        appCompatImageView_food = dieView.findViewById(R.id.AppCompatImageView_food);
        categoryHEAD = new String[] {"CATEGORY"};
        categoryCELL = new int[] {R.id.categoryname};
        foodHEAD = new String[]{"FOOD NAME"};
        foodCELL = new int[] {R.id.foodname};
        foodSearchHEAD  = new String[]{"FOOD INFO"};
        foodSearchCELL = new int[] {R.id.food_info};
        categoryList = dieView.findViewById(R.id.category_list);
        foodList = dieView.findViewById(R.id.food_list);
        foodSearchList = dieView.findViewById(R.id.new_food_list);
        categoryArray = new ArrayList<HashMap<String, String>>();
        TextView_description = dieView.findViewById(R.id.TextView_description);
        GetCategoryAsyncTask getCategoryAsyncTask = new GetCategoryAsyncTask();
        getCategoryAsyncTask.execute();
        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GetCategoryAsyncTask2 getCategoryAsyncTask2 = new GetCategoryAsyncTask2();
                List<String> tmpList = null;
                try {
                    tmpList = getCategoryAsyncTask2.execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String category = tmpList.get(i);
                FindFoodNameByCategoryAsyncTask findFoodNameByCategory = new FindFoodNameByCategoryAsyncTask();
                findFoodNameByCategory.execute(category);
                //TextView_description.setText(category);
                //Toast.makeText(ListViewActivity.this,book.toString(),Toast.LENGTH_LONG).show();
            }
        });

        foodList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GetFoodAsyncTask getFoodAsyncTask = new GetFoodAsyncTask();
                HashMap<String,String> map = (HashMap<String,String>)foodList.getItemAtPosition(position);
                String food_name = map.get("FOOD NAME");
                getFoodAsyncTask.execute(food_name);
            }
        });

        foodSearchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //GetFoodAsyncTask getFoodAsyncTask = new GetFoodAsyncTask();
                String category = "";
                HashMap<String, String> map = (HashMap<String, String>) foodSearchList.getItemAtPosition(position);
                String foodInfo = map.get("FOOD INFO");
                String[] infoSegment = foodInfo.split(",");
                //2,4,5,7,10 use to ge detial
                String foodName = infoSegment[1];
                double serving = Double.valueOf(infoSegment[3]);
                String unit = infoSegment[4];
                Long calorieAmount = Long.valueOf(infoSegment[6]);
                String i = infoSegment[9];
                double fat = Double.valueOf(infoSegment[9]);
                newFoodSelected = new Food(foodName,category,calorieAmount,unit, (int) serving, (long) fat);
                ImageTask imageTask = new ImageTask();
                imageTask.execute(newFoodSelected.getFname());
                gSearchAsyncTask gSearch = new gSearchAsyncTask();
                gSearch.execute(newFoodSelected.getFname());
                //getFoodAsyncTask.execute(food_name);
            }
        });
        AppCompatButton appCompatButtonAddnetbean = dieView.findViewById(R.id.appCompatButtonAddnetbean);
        appCompatButtonAddnetbean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textInputEditTextCategory.getText().toString().isEmpty()){
                    textInputEditTextCategory.setError("Category of new food cannot not be null!");
                    textInputEditTextCategory.requestFocus();
                }else{
                    newFoodSelected.setCategory(textInputEditTextCategory.getText().toString().trim().toUpperCase());
                    AddFoodAsyncTask addFoodAsyncTask = new AddFoodAsyncTask();
                    addFoodAsyncTask.execute(newFoodSelected);
                }
            }
        });
        AppCompatButton appCompatButtonAdd = dieView.findViewById(R.id.appCompatButtonAdd);
        appCompatButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textInputEditTextServe.getText().toString().isEmpty()) {
                    textInputEditTextServe.setError("Serving amount cannot be null!");
                    textInputEditTextServe.requestFocus();
                }else{
                    Food foodid = foodSelected;
                    DieFragment.ReadDatabase readDatabase = new DieFragment.ReadDatabase();
                    readDatabase.execute(foodid);
                }
            }
        });
        AppCompatButton appCompatButtonSearch = dieView.findViewById(R.id.appCompatButtonSearch);
        appCompatButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textInputEditTextSearch.getText().toString().isEmpty()) {
                    textInputEditTextSearch.setError("Enter food name for search");
                    textInputEditTextSearch.requestFocus();
                } else{
                    String foodName = textInputEditTextSearch.getText().toString();
                    foodName.replace(" ","-");
                    SearchFatAsyncTask searchFatAsyncTask = new SearchFatAsyncTask();
                    searchFatAsyncTask.execute(foodName);
                }
            }
        });
        return dieView;
    }

    private class ReadDatabase extends AsyncTask<Food, Void, List> {
        @Override
        protected List doInBackground(Food... params) {
            List<Cuser> users = RestClient.findAllCuser();
            Cuser cuser = new Cuser();
            for (Cuser temp : users) {
                if (temp.getUsid() == id){
                    cuser = temp;
                }
            }
            List objects = new ArrayList<>();
            objects.add(cuser);
            objects.add(params[0]);
            return objects; }
        @Override
        protected void onPostExecute(List objects) {
            Cuser userid =(Cuser) objects.get(0);
            Food foodid = (Food) objects.get(1);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            String dateString = sdf.format(new Date());
            Consumption consumption = new Consumption(dateString,0,Integer.parseInt(textInputEditTextServe.getText().toString()),foodid,userid);
            AddConsumptionAsyTask addConsumptionAsyTask = new AddConsumptionAsyTask();
            addConsumptionAsyTask.execute(consumption);
        }
    }

    private class FindFoodNameByCategoryAsyncTask extends AsyncTask<String, Void, List<String>> {
        @Override
        protected List<String> doInBackground(String... params) {
            //SONObject result;
            foodArray = new ArrayList<HashMap<String, String>>();
            List<String> foodNameList = new ArrayList<String>();
            foodNameList = RestClient.FindByCategory(params[0]);
            return foodNameList;
        }

        protected void onPostExecute(List<String> foodNameList) {
            for (String foodName:foodNameList) {
                HashMap<String,String> foodNameMap = new HashMap<String,String>();
                foodNameMap.put("FOOD NAME",foodName);
                foodArray.add(foodNameMap);
            }

            foodNameAdapter = new SimpleAdapter(getActivity(),foodArray,R.layout.foodlist,foodHEAD,foodCELL);
            foodList.setAdapter(foodNameAdapter);
        }
    }



    private class GetCategoryAsyncTask extends AsyncTask<Void, Void, List<String>> {
        @Override
        protected List<String> doInBackground(Void... params) {
            //SONObject result;
            List<String> categoryTmp = new ArrayList<String>();
            categoryTmp = RestClient.findAllCategory();
            return categoryTmp;
        }

        @Override
        protected void onPostExecute(List<String> categoryTmp) {
            for (String category:categoryTmp) {
                HashMap<String,String> cateMap = new HashMap<String,String>();
                cateMap.put("CATEGORY",category);
                categoryArray.add(cateMap);
            }

            categoryAdapter = new SimpleAdapter(getActivity(),categoryArray,R.layout.categorylist,categoryHEAD,categoryCELL);
            categoryList.setAdapter(categoryAdapter);
        }
    }

    private class GetCategoryAsyncTask2 extends AsyncTask<Void, Void, List<String>> {
        @Override
        protected List<String> doInBackground(Void... params) {
            //SONObject result;
            List<String> categoryTmp = new ArrayList<String>();
            categoryTmp = RestClient.findAllCategory();
            return categoryTmp;
        }
    }

    private class GetFoodAsyncTask extends AsyncTask<String, Void, Food> {
        @Override
        protected Food doInBackground(String... params) {
            List<Food> foodList = new ArrayList<>();
            foodList=RestClient.findAllFood();
            for (Food food:foodList){
                if (food.getFname().equals(params[0]))
                {
                    foodSelected = food;
                    return food;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Food foodSelected) {
            textView_Selected = dieView.findViewById(R.id.textView_Selected);
            String foodSelectedInfo = "Food Name: " + foodSelected.getFname() + " , Food Calorie: " + foodSelected.getCamount()
                    + " kcal, Food fat: " + foodSelected.getFat() + " g";
            textView_Selected.setText(foodSelectedInfo);
        }
    }

    private class AddConsumptionAsyTask extends AsyncTask<Consumption,Void,String>{

        @Override
        protected String doInBackground(Consumption... consumptions) {
            try {
                List<Consumption> consumptionList = RestClient.findAllConsumption();
                int i = 0;
                for (Consumption consumption:consumptionList)
                {
                    if (consumption.getConsumpid()>i)
                    {
                        i = consumption.getConsumpid();
                    }
                }
                consumptions[0].setConsumpid(i+1);
                RestClient.createConsumption(consumptions[0]);
                return "Add Successfully!";
            }catch (Exception e){
                e.printStackTrace();
                return "Add too database fail!";
            }
        }
        @Override
        protected void onPostExecute(String message){
            Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
        }
    }

    private class SearchFatAsyncTask extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject result;
            result = FatSecretGet.getFood(params[0]);

            return result;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            TextView foodDes = dieView.findViewById(R.id.TextView_description);
            String resultJArrayStr = "";
            foodSearchArray  = new ArrayList<HashMap<String, String>>();
            List<String> allFoodList = new ArrayList<String>();
            //TextView searchFatView = dieView.findViewById(R.id.);
            try {
                resultJArrayStr = result.getString("food");
                JSONArray foodsJArray = new JSONArray(resultJArrayStr);
                for (int i = 0; i < foodsJArray.length(); i++){
                    JSONObject food = (JSONObject) foodsJArray.get(i);
                    String foodName = food.getString("food_name");
                    String foodInfo = food.getString("food_description");
                    String[] infoList = foodInfo.split("\\|");

                    String keyInfo = infoList[0];
                    String[] keyValueList = keyInfo.split("-");
                    String amountAndUnit = keyValueList[0];
                    String calorieStr = keyValueList[1];
                    String fatStr = infoList[1];
                    double servingAmount = ExtractNumber(amountAndUnit);
                    String unit = ExtractUnit(amountAndUnit);
                    int calorie = (int)(ExtractNumber (calorieStr));
                    double fat = ExtractNumber(fatStr);
                    String foodAllInfo = "Food Name," + foodName + ", Per ," + servingAmount + "," + unit
                            + ", Calorie ," + calorie + ", kcal, Fat , " + fat + ",g";

                    allFoodList.add(foodAllInfo);

                }
                for (String foodInfomation : allFoodList){
                    HashMap<String,String> foodInfoMap = new HashMap<String,String>();
                    foodInfoMap.put("FOOD INFO",foodInfomation);
                    foodSearchArray.add(foodInfoMap);
                }
                foodSearchAdapter = new SimpleAdapter(getActivity(),foodSearchArray,R.layout.searchlist,foodSearchHEAD,foodSearchCELL);
                foodSearchList.setAdapter(foodSearchAdapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class gSearchAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            JSONObject result;
            return SearchGoogleAPI.search(params[0], new String[]{"num"}, new
                    String[]{"1"});
            //return result;
        }

        @Override
        protected void onPostExecute(String result) {
            //TextView tv= (TextView) findViewById(R.id.searchResult);
            //TextView_description = dieView.findViewById(R.id.food);
            //String result_String = result;
            TextView_description.setText(SearchGoogleAPI.getSnippet(result));
        }
    }

    private class ImageTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... keyword) {
            String info = SearchGoogleAPI.search(keyword[0], new String[]{"num"}, new
                    String[]{"1"});
            String urldisplay = SearchGoogleAPI.getImageLink(info);
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            appCompatImageView_food.setImageBitmap(result);
        }
    }

    private class AddFoodAsyncTask extends AsyncTask<Food, Void, String>
    {
        @Override
        protected String doInBackground (Food...foods){
            try {
                Food addFood = foods[0];
                List<Food> foodList = RestClient.findAllFood();
                int foodid = 0;
                for (Food food:foodList)
                {
                    if (food.getFid()>foodid)
                    {
                        foodid = food.getFid();
                    }
                }
                addFood.setFid(foodid+1);
                RestClient.createFood(addFood);
            }catch (Exception e){
                e.printStackTrace();
                return "Add food fail";
            }
            return "Add food successfully";
        }
        @Override
        protected void onPostExecute (String message){
            Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false);
            }
            ft.detach(new DieFragment()).attach(new DieFragment()).commit();
            //TextView resultTextView = (TextView) findViewById(R.id.tvResult);
            //resultTextView.setText(message);
        } }
    public static String ExtractUnit(String str){
        Pattern unitPattern = Pattern.compile("\\d+([ A-Za-z]+)");
        Matcher unitMatch = unitPattern.matcher(str);
        if(unitMatch.find()){
            String unit = unitMatch.group(1);
            return unit;
        }
        return null;
    }

    public static double ExtractNumber(String str){
        Pattern amountPattern = Pattern.compile("(\\d+\\.?\\d?)");
        Matcher amountMatch = amountPattern.matcher(str);
        if(amountMatch.find()){
            double number = Double.parseDouble(amountMatch.group());
            return number;
        }
        return 0.0;
    }
}
