package com.example.xukaijun.login.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.arch.persistence.room.Room;
import com.example.xukaijun.login.MainActivity;
import com.example.xukaijun.login.R;
import com.example.xukaijun.login.model.User;
import com.example.xukaijun.login.model.UserDatabase;
import com.example.xukaijun.login.sql.RestClient;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainFragment extends Fragment {
    UserDatabase db = null;
    private TextView textViewwelcome,textView_consumption,textView_burn;
    private TextView textViewGoalstep;
    private TextInputLayout textInputLayoutGoalstep;
    private TextInputEditText textInputEditGoalstep;
    private int id;
    View vMain;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        db = Room.databaseBuilder((MainActivity)getContext(),
                UserDatabase.class, "UserDatabase") .fallbackToDestructiveMigration()
                .build();
        id =  ((MainActivity)getActivity()).getId();
        vMain = inflater.inflate(R.layout.fragment_main, container, false);
        textViewwelcome = vMain.findViewById(R.id.textView_welcome);
        textView_consumption = vMain.findViewById(R.id.textView_consumption);
        textView_burn = vMain.findViewById(R.id.textView_burn);
        textViewGoalstep= vMain.findViewById(R.id.textView_Goalstep);
        textInputLayoutGoalstep = (TextInputLayout) vMain.findViewById(R.id.textInputLayoutGoalstep);
        textInputEditGoalstep = (TextInputEditText) vMain.findViewById(R.id.textInputEditTextGoalstep);
        ConsumptionTask consumptionTask = new ConsumptionTask();
        consumptionTask.execute();
        ReadDatabase readDatabase = new ReadDatabase();
        readDatabase.execute();
        Button goalstepButton = (Button)vMain.findViewById(R.id.appCompatButtonGoalstep);
        goalstepButton.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                if (textInputEditGoalstep.getText().toString().isEmpty()){
                    textInputEditGoalstep.setError("Cannot not be null!");
                }else {
                UpdateDatabase updateDatabase = new UpdateDatabase();
                updateDatabase.execute();}
            }
        });
        return vMain;
    }
    private class ReadDatabase extends AsyncTask<Void, Void, User> {
        @Override
    protected User doInBackground(Void... params) {
        List<User> users = db.userDao().getAll();
        User user = new User();
        for (User temp : users) {
            if (temp.getId() == id){
                user = temp;
            }
        }
        return user; }
        @Override
        protected void onPostExecute(User user1) { textViewwelcome.setText("Welcome to Calorie Tracker "+user1.getFirstName());
            textInputLayoutGoalstep.setHint(String.valueOf(user1.getGoalsteps()));
        }
    }
    private class UpdateDatabase extends AsyncTask<Void, Void, User> {
        @Override protected User doInBackground(Void... params) {
            String goal = textInputEditGoalstep.getText().toString();
            List<User> users = db.userDao().getAll();
            User user = new User();
            for (User temp : users) {
                if (temp.getId() == id){
                    user = temp;
                    user.setGoalsteps(Integer.valueOf(goal));
                }
            }
            if (user!=null) {
                db.userDao().updateUsers(user);
                return user; }
            return null; }
        @Override
        protected void onPostExecute(User user1) { textInputLayoutGoalstep.setHint(String.valueOf(user1.getGoalsteps()));
            textViewGoalstep.setText("The Calorie Goal have changed to " + user1.getGoalsteps() + " kcal");
        }
    }
    private class ConsumptionTask extends AsyncTask<Void, Void, List<String>> {
        @Override
        protected List<String> doInBackground(Void... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            List<String> info = new ArrayList<>();
            String dateString = sdf.format(new Date());
            String burn = String.valueOf(RestClient.findtCalories(id)+RestClient.findcaloriePerStep(id)*db.userDao().findByID(id).getTotalsteps());
            info.add(RestClient.findConsumptionByUidandDate(id,dateString));
            info.add(burn);
            return info;
        }

        @Override
        protected void onPostExecute(List<String> response) {
            textView_consumption.setText(response.get(0)+ " kcal");
            textView_burn.setText(response.get(1)+ " kcal");
        }
    }
}
