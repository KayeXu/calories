package com.example.xukaijun.login.model;

import java.util.Date;


public class Cuser {
    private Integer usid;
    private String uname;
    private String usurname;
    private String uemail;
    private Date dob;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    private Double uheight;
    private Double uweight;
    private String ugender;
    private String uaddress;
    private Short upostcode;
    private Integer levelofactivity;
    private Long stepspermile;
    public Cuser() {
    }

    public Cuser(Integer usid, String uname, String usurname, String uemail, Date dob, Double uheight, Double uweight, String ugender, String uaddress, Short upostcode, Integer levelofactivity, Long stepspermile) {
        this.usid = usid;
        this.uname = uname;
        this.usurname = usurname;
        this.uemail = uemail;
        this.dob = dob;
        this.uheight = uheight;
        this.uweight = uweight;
        this.ugender = ugender;
        this.uaddress = uaddress;
        this.upostcode = upostcode;
        this.levelofactivity = levelofactivity;
        this.stepspermile = stepspermile;
    }

    public Cuser(Integer usid) {
        this.usid = usid;
        this.uname = " ";
        this.usurname = " ";
        this.uemail = " ";
        this.dob = new Date();
        this.uheight = 0.0;
        this.uweight = 0.0;
        this.ugender = " ";
        this.uaddress = " ";
        this.upostcode = 0;
        this.levelofactivity = 0;
        this.stepspermile = 0L;


    }


    public Integer getUsid() {
        return usid;
    }

    public void setUsid(Integer usid) {
        this.usid = usid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUsurname() {
        return usurname;
    }

    public void setUsurname(String usurname) {
        this.usurname = usurname;
    }

    public String getUemail() {
        return uemail;
    }

    public void setUemail(String uemail) {
        this.uemail = uemail;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Double getUheight() {
        return uheight;
    }

    public void setUheight(Double uheight) {
        this.uheight = uheight;
    }

    public Double getUweight() {
        return uweight;
    }

    public void setUweight(Double uweight) {
        this.uweight = uweight;
    }

    public String getUgender() {
        return ugender;
    }

    public void setUgender(String ugender) {
        this.ugender = ugender;
    }

    public String getUaddress() {
        return uaddress;
    }

    public void setUaddress(String uaddress) {
        this.uaddress = uaddress;
    }

    public Short getUpostcode() {
        return upostcode;
    }

    public void setUpostcode(Short upostcode) {
        this.upostcode = upostcode;
    }

    public Integer getLevelofactivity() {
        return levelofactivity;
    }

    public void setLevelofactivity(Integer levelofactivity) {
        this.levelofactivity = levelofactivity;
    }

    public Long getStepspermile() {
        return stepspermile;
    }

    public void setStepspermile(Long stepspermile) {
        this.stepspermile = stepspermile;
    }
}
