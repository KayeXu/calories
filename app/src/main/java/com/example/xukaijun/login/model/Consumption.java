package com.example.xukaijun.login.model;

public class Consumption {
    /**
     * cdate : 2019-03-20T00:00:00+11:00
     * consumpid : 1
     * cquantity : 100.0
     * fid : {"camount":84,"category":"Fruit","fat":4,"fid":1,"fname":"apple","samount":1,"sunit":"each"}
     * usid : {"dob":"1995-02-09T00:00:00+11:00","levelofactivity":3,"stepspermile":2200,"uaddress":"Blackburn rd","uemail":"kxuu0012@student.monash.edu","ugender":"M","uheight":180,"uname":"kaye","upostcode":3168,"usid":1,"usurname":"Xu","uweight":100}
     */

    private String cdate;
    private int consumpid;
    private double cquantity;
    private Food fid;
    private Cuser usid;

    public Consumption() {
    }

    public Consumption(String cdate, int consumpid, double cquantity, Food fid, Cuser usid) {
        this.cdate = cdate;
        this.consumpid = consumpid;
        this.cquantity = cquantity;
        this.fid = fid;
        this.usid = usid;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public int getConsumpid() {
        return consumpid;
    }

    public void setConsumpid(int consumpid) {
        this.consumpid = consumpid;
    }

    public double getCquantity() {
        return cquantity;
    }

    public void setCquantity(double cquantity) {
        this.cquantity = cquantity;
    }

    public Food getFid() {
        return fid;
    }

    public void setFid(Food fid) {
        this.fid = fid;
    }

    public Cuser getUsid() {
        return usid;
    }

    public void setUsid(Cuser usid) {
        this.usid = usid;
    }
}
