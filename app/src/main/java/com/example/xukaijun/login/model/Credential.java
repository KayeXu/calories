package com.example.xukaijun.login.model;

import java.util.Date;

public class Credential {
    private String cusername;
    private String cpassword;
    private Date sdate;
    private Cuser usid;
    public Credential() {
    }

    public Credential(String cusername) {
        this.cusername = cusername;
    }

    public Credential(String cusername, String cpassword, Date sdate, Cuser usid) {
        this.cusername = cusername;
        this.cpassword = cpassword;
        this.sdate = sdate;
        this.usid = usid;
    }

    public String getCusername() {
        return cusername;
    }

    public void setCusername(String cusername) {
        this.cusername = cusername;
    }

    public String getCpassword() {
        return cpassword;
    }

    public void setCpassword(String cpassword) {
        this.cpassword = cpassword;
    }

    public Date getSdate() {
        return sdate;
    }

    public void setSdate(Date sdate) {
        this.sdate = sdate;
    }

    public Cuser getUsid() {
        return usid;
    }

    public void setUsid(Cuser usid) {
        this.usid = usid;
    }
}
