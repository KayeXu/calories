package com.example.xukaijun.login.activities;

import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.xukaijun.login.MainActivity;
import com.example.xukaijun.login.R;
import com.example.xukaijun.login.helpers.HttpUrlHelper;
import com.example.xukaijun.login.model.Credential;
import com.example.xukaijun.login.model.Cuser;
import com.example.xukaijun.login.sql.RestClient;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.example.xukaijun.login.activities.LoginActivity.getDateFromDatePicker;

public class ReportFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    private int id;
    private PieChart pieChart;
    private BarChart barChart;
    private DatePicker datePickerSelect,datePickerStart,datePickerEnd;
    private YAxis leftAxis;             //左侧Y轴
    private XAxis xAxis;                //X轴
    public ReportFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        datePickerSelect = (DatePicker) view.findViewById(R.id.DatePickerSelect);
        datePickerStart = (DatePicker) view.findViewById(R.id.DatePickerStart);
        datePickerEnd = (DatePicker) view.findViewById(R.id.DatePickerEnd);
        pieChart = view.findViewById(R.id.pie_chart);
        Button appCompatButtonPie = (Button) view.findViewById(R.id.appCompatButtonPie);
        appCompatButtonPie.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                PieAsyncTask  pieAsyncTask = new PieAsyncTask();
                pieAsyncTask.execute();
            }
        });
        barChart = view.findViewById(R.id.bar_chart);
        Button appCompatButtonBar = (Button) view.findViewById(R.id.appCompatButtonBar);
        appCompatButtonBar.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                BarAsyncTask  barAsyncTask = new BarAsyncTask();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                List<String> dataList = new ArrayList<String>();
                String startDate = sdf.format(getDateFromDatePicker(datePickerStart));
                String endDate = sdf.format(getDateFromDatePicker(datePickerEnd));
                String dateStr = "";
                xAxis = barChart.getXAxis();
                leftAxis = barChart.getAxisLeft();
                xAxis.setGranularity(1f);
                xAxis.setCenterAxisLabels(true);
                List<String> xLaleList = new ArrayList<String>();
                List<BarEntry> yVals1 = new ArrayList<BarEntry>();
                List<BarEntry> yVals2 = new ArrayList<BarEntry>();
                try {
                    dataList = barAsyncTask.execute(startDate,endDate).get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < dataList.size(); i += 3) {
                    String cDate = dataList.get(i + 2);
                    try {
                        dateStr = sdf.format(sdf.parse(cDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    yVals1.add(new BarEntry(i, Float.parseFloat(dataList.get(i + 1))));
                    yVals2.add(new BarEntry(i, Float.parseFloat(dataList.get(i))));
                    xLaleList.add(dataList.get(i + 2));
                }
                BarDataSet set1, set2;
                if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
                    set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
                    set2 = (BarDataSet) barChart.getData().getDataSetByIndex(1);
                    set1.setValues(yVals1);
                    set2.setValues(yVals2);
                    barChart.getData().notifyDataChanged();
                    barChart.notifyDataSetChanged();
                } else {
                    set1 = new BarDataSet(yVals1, "totalCalConsumed");
                    set1.setColor(Color.rgb(104, 241, 175));
                    set2 = new BarDataSet(yVals2, "totalCalBurned");
                    set2.setColor(Color.rgb(164, 228, 251));
                    ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
                    dataSets.add(set1);
                    dataSets.add(set2);
                    BarData data = new BarData(dataSets);
                    barChart.setData(data);
                    barChart.getBarData().setBarWidth(0.45f);
                    barChart.groupBars(0, 0.1f, 0.03f);
                    barChart.setDrawBarShadow(false);
                    barChart.setDrawValueAboveBar(true);
                    barChart.setDrawGridBackground(true);
                    leftAxis.setAxisMinimum(0f);
                    xAxis.setValueFormatter(new IndexAxisValueFormatter(xLaleList));
                    barChart.getDescription().setEnabled(false);
                    barChart.getAxisRight().setEnabled(false);
                    barChart.getLegend().setEnabled(false);
                    barChart.setScaleEnabled(false);
                    barChart.getData().setHighlightEnabled(false);
                    barChart.animateY(1000);
                    barChart.getXAxis().setDrawGridLines(false);
                    xAxis.setCenterAxisLabels(true);
                    barChart.getAxisLeft().setDrawLabels(true);
                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                    xAxis.setGranularity(1);
                    xAxis.setLabelRotationAngle(-45);
                    barChart.invalidate();
                }
            }
        });
        return view;
    }

    private class PieAsyncTask extends AsyncTask<String, Void, Object> {
        @Override
        protected Object doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String reportDate = sdf.format(getDateFromDatePicker(datePickerSelect));
            id =  ((MainActivity)getActivity()).getId();
            String userid = String.valueOf(id);
            String url = "http://118.138.4.208:8080/Calorie/webresources/calories.report/caloriesReport/"+ userid +"/"+ reportDate;
            Object dataTransfer[] = new Object[1];
            dataTransfer[0] = url;
            return dataTransfer[0];
        }

        @Override
        protected void onPostExecute(Object dataTransfer) {
            new GetReportData().execute(dataTransfer);
            pieChart.getDescription().setEnabled(false);
            pieChart.setCenterText(generateCenterText());
            pieChart.setCenterTextSize(10f);
            // radius of the center hole in percent of maximum radius
            pieChart.setHoleRadius(45f);
            pieChart.setTransparentCircleRadius(50f);
            pieChart.getLegend().setEnabled(false);
            pieChart.animateX(1000);
            Toast.makeText(getActivity(),"success",Toast.LENGTH_SHORT).show();
        }
    }

    private class BarAsyncTask extends AsyncTask<String, Void, List<String>> {
        @Override
        protected List<String> doInBackground(String... params) {
            id =  ((MainActivity)getActivity()).getId();
            String userid = String.valueOf(id);
            List<String> datelist = new ArrayList<>();
            List<String> resultList = new ArrayList<String>();
            try {
                datelist = getBetweenDays(params[0], params[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (String dateStr : datelist) {
                String result = RestClient.findReportByUidandDate(id, dateStr);
                JSONObject resultJSON = null;
                try {
                    resultJSON = new JSONObject(result);
                    double totalConsumed = resultJSON.getDouble("Total calories consumed");
                    double totalBurn = resultJSON.getDouble("Total calories borned");
                    DecimalFormat df = new DecimalFormat("0.000 ");
                    resultList.add(String.valueOf(Double.parseDouble(df.format(totalConsumed))));
                    resultList.add(String.valueOf(Double.parseDouble(df.format(totalBurn))));
                    resultList.add(dateStr);
                }catch(JSONException e){
                    e.printStackTrace();
                    resultList.add("0");
                    resultList.add("0");
                    resultList.add(dateStr);
                }
            }
            return resultList;
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private SpannableString generateCenterText() {
        SpannableString s = new SpannableString("Goal\n  3000 cal");
        s.setSpan(new RelativeSizeSpan(2f), 0, 8, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 8, s.length(), 0);
        return s;
    }

    public class GetReportData extends AsyncTask<Object,String,String> {
        @Override
        protected String doInBackground(Object... objects) {

            //Log.w("Report","I'm here!");
            String url = (String) objects[0];
            String data = new HttpUrlHelper().getHttpUrlConnection(url);
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONObject dataObj = new JSONObject(s);
                generatePieData(dataObj);
            } catch (JSONException e) {
                Toast.makeText(getActivity(),"Not data in this day",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            } catch (NullPointerException e){
                e.printStackTrace();
            }

        }
    }
    private PieData generatePieData(JSONObject data) throws JSONException {
        double totalConsumed = data.getDouble("Total calories consumed");
        double totalBurned = data.getDouble("Total calories borned");
        int remaining = data.getInt("remaining calorie");
        ArrayList<PieEntry> entries1 = new ArrayList<>();
        entries1.add(new PieEntry((float) totalConsumed));
        entries1.add(new PieEntry((float) totalBurned));
        //entries1.add(new PieEntry(remaining));
        PieDataSet ds1 = new PieDataSet(entries1, "Goal 2000");
        ds1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.WHITE);
        ds1.setValueTextSize(12f);
        PieData pieData = new PieData(ds1);
        pieChart.setData(pieData);
        return pieData;
    }
    public static  List<String> getBetweenDays(String stime,String etime){
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        Date sdate=null;
        Date eDate=null;
        try {
            sdate=df.parse(stime);
            eDate=df.parse(etime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        long betweendays=(long) ((eDate.getTime()-sdate.getTime())/(1000 * 60 * 60 *24)+0.5);//天数间隔
        Calendar c = Calendar.getInstance();
        List<String> list=new ArrayList<String>();
        while (sdate.getTime()<=eDate.getTime()) {
            list.add(df.format(sdate));
            System.out.println(df.format(sdate));
            c.setTime(sdate);
            c.add(Calendar.DATE, 1); // 日期加1天
            sdate = c.getTime();
        }
        return list;
    }
}
