package com.example.xukaijun.login.activities;

import android.os.Build;
import android.support.v4.app.Fragment;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.xukaijun.login.MainActivity;
import com.example.xukaijun.login.R;
import com.example.xukaijun.login.model.Consumption;
import com.example.xukaijun.login.model.Cuser;
import com.example.xukaijun.login.model.Report;
import com.example.xukaijun.login.model.Step;
import com.example.xukaijun.login.model.StepDatabase;
import com.example.xukaijun.login.model.User;
import com.example.xukaijun.login.model.UserDatabase;
import com.example.xukaijun.login.sql.RestClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class StepFragment extends Fragment {
    UserDatabase db = null;
    StepDatabase db1 = null;
    private TextView textViewTotalstep;
    private TextView textView_eachstep;
    private TextInputLayout textInputLayoutTotalstep;
    private TextInputEditText textInputEditTotalstep;
    private TextInputLayout textInputLayoutEachstep;
    private TextInputEditText textInputEditEachstep;
    private int id;
    View vMain;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        db = Room.databaseBuilder((MainActivity) getContext(),
                UserDatabase.class, "UserDatabase").fallbackToDestructiveMigration()
                .build();
        db1 = Room.databaseBuilder((MainActivity) getContext(),
                StepDatabase.class, "StepDatabase").fallbackToDestructiveMigration()
                .build();
        id = ((MainActivity) getActivity()).getId();
        vMain = inflater.inflate(R.layout.fragment_step, container, false);
        textViewTotalstep = vMain.findViewById(R.id.textView_Totalstep);
        textInputLayoutTotalstep = (TextInputLayout) vMain.findViewById(R.id.textInputLayoutTotalstep);
        textInputEditTotalstep = (TextInputEditText) vMain.findViewById(R.id.textInputEditTextTotalstep);
        textView_eachstep = vMain.findViewById(R.id.textView_eachstep);
        textInputLayoutEachstep = (TextInputLayout) vMain.findViewById(R.id.textInputLayoutEachstep);
        textInputEditEachstep = (TextInputEditText) vMain.findViewById(R.id.textInputEditTextEachstep);
        StepFragment.ReadDatabase readDatabase = new StepFragment.ReadDatabase();
        readDatabase.execute();
        Button addButton = (Button) vMain.findViewById(R.id.appCompatButtonAddstep);
        addButton.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                if (textInputEditTotalstep.getText().toString().isEmpty()){
                    textInputEditTotalstep.setError("Cannot not be null!");
                }else{
                StepFragment.UpdateDatabase updateDatabase = new StepFragment.UpdateDatabase();
                updateDatabase.execute(textInputEditTotalstep.getText().toString());
                }
            }
        });
        Button clearButton = (Button) vMain.findViewById(R.id.appCompatButtonClearstep);
        clearButton.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                StepFragment.ClearDatabase clearDatabase = new StepFragment.ClearDatabase();
                clearDatabase.execute("0");
            }
        });
        Button editstep = (Button) vMain.findViewById(R.id.appCompatButtonEachstep);
        editstep.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                StepFragment.UpdateStepDatabase stepDatabase = new StepFragment.UpdateStepDatabase();
                stepDatabase.execute();
            }
        });
        Button submit = (Button) vMain.findViewById(R.id.appCompatButtonSubmit);
        submit.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                UpdateNetbean updateNetbean = new UpdateNetbean();
                updateNetbean.execute();
            }
        });
        return vMain;
    }

    private class ReadDatabase extends AsyncTask<Void, Void, List<String>> {
        @Override
        protected List<String> doInBackground(Void... params) {
            List<String> info = new ArrayList<>();
            List<User> users = db.userDao().getAll();
            User user = new User();
            for (User temp : users) {
                if (temp.getId() == id) {
                    user = temp;
                }
            }
            info.add(String.valueOf(user.getTotalsteps()));
            List<Step> steps = db1.stepDao().getAll();
            for (Step temp : steps) {
                if (temp.getUid() == id) {
                    Step step = new Step();
                    step = temp;
                    info.add("id: " + step.getStepid() + ", steps: " + step.getSteps() + ", time: " + step.getInserttime() + "\n");
                }
            }
            return info;
        }

        @Override
        protected void onPostExecute(List<String> user1) {
            textInputLayoutTotalstep.setHint(user1.get(0));
            textViewTotalstep.setText("The steps have changed to 0");
            String info = "All info: ";
            for (int i = 1; i < user1.size(); i++) {
                info = info + user1.get(i);
            }
            textView_eachstep.setText(info);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false);
            }
            ft.detach(new StepFragment()).attach(new StepFragment()).commit();
        }
    }

    private class UpdateDatabase extends AsyncTask<String, Void, List<String>> {
        @Override
        protected List<String> doInBackground(String... params) {
            String goal = params[0];
            List<String> info = new ArrayList<>();
            List<User> users = db.userDao().getAll();
            User user = new User();
            for (User temp : users) {
                if (temp.getId() == id) {
                    user = temp;
                    user.setTotalsteps(Integer.valueOf(goal) + user.getTotalsteps());
                }
            }
            if (user != null) {
                db.userDao().updateUsers(user);
                info.add(String.valueOf(user.getTotalsteps()));
            }
            int flag = 0;
            List<Step> steps = db1.stepDao().getAll();
            for (Step temp : steps) {
                if (temp.getStepid() > flag) {
                    flag = temp.getStepid();
                }
                if (temp.getUid() == id) {
                    Step step = new Step();
                    step = temp;
                    info.add("id: " + step.getStepid() + ", steps: " + step.getSteps() + ", time: " + step.getInserttime() + "\n");
                }
            }
            Step step3 = new Step();
            step3.setStepid(flag + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
            String dateString = sdf.format(new Date());
            step3.setInserttime(dateString);
            step3.setSteps(Integer.valueOf(goal));
            step3.setUid(id);
            info.add("id: " + step3.getStepid() + ", steps: " + step3.getSteps() + ", time: " + step3.getInserttime() + "\n");
            db1.stepDao().insert(step3);
            return info;
        }

        @Override
        protected void onPostExecute(List<String> user1) {
            textInputLayoutTotalstep.setHint(user1.get(0));
            textViewTotalstep.setText("The steps have changed to " + user1.get(0));
            String info = "All info: ";
            for (int i = 1; i < user1.size(); i++) {
                info = info + user1.get(i);
            }
            textView_eachstep.setText(info);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false);
            }
            ft.detach(new StepFragment()).attach(new StepFragment()).commit();
        }
    }

    private class UpdateNetbean extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            List<String> info = new ArrayList<>();
            List<Report> reportList = RestClient.findAllReport();
            int i = 0;
            for (Report report1 : reportList) {
                if (report1.getRid() > i) {
                    i = report1.getRid();
                }
            }
            List<Cuser> cuserList = RestClient.findAllCuser();
                for (Cuser cuser : cuserList) {
                        Report report = new Report();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                        String dateString = sdf.format(new Date());
                        report.setCdate(dateString);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                    String dateString1 = sdf1.format(new Date());
                    report.setCdate(dateString);
                        User user = db.userDao().findByID(cuser.getUsid());
                        if (user == null){
                        continue;
                        }
                        report.setRid(i+1);
                        i=i+1;
                        report.setTotalsteps(user.getTotalsteps());
                        report.setRgoals(user.getGoalsteps());
                        report.setTotalconsumed(Double.valueOf(RestClient.findConsumptionByUidandDate(cuser.getUsid(), dateString1)));
                        report.setTotalburned(RestClient.findtCalories(cuser.getUsid()) + RestClient.findcaloriePerStep(cuser.getUsid()) * db.userDao().findByID(cuser.getUsid()).getTotalsteps());
                        report.setUsid(cuser);
                        RestClient.createReport(report);
                        user.setTotalsteps(0);
                        user.setGoalsteps(0);
                        db1.stepDao().deleteByUID(cuser.getUsid());
                        db.userDao().updateUsers(user);
                    }
            return "finish";
        }

        @Override
        protected void onPostExecute(String user1) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false);
            }
            ft.detach(new StepFragment()).attach(new StepFragment()).commit();
        }
    }

    private class ClearDatabase extends AsyncTask<String, Void, User> {
        @Override
        protected User doInBackground(String... params) {
            String goal = params[0];
            List<User> users = db.userDao().getAll();
            User user = new User();
            for (User temp : users) {
                if (temp.getId() == id) {
                    user = temp;
                    user.setTotalsteps(Integer.valueOf(goal));
                }
            }
            db1.stepDao().deleteByUID(id);
            if (user != null) {
                db.userDao().updateUsers(user);
                return user;
            }
            return null;
        }

        @Override
        protected void onPostExecute(User user1) {
            textInputLayoutTotalstep.setHint(String.valueOf(user1.getTotalsteps()));
            textViewTotalstep.setText("The steps have changed to " + user1.getTotalsteps());
            textView_eachstep.setText("All info: ");
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false);
            }
            ft.detach(new StepFragment()).attach(new StepFragment()).commit();
        }
    }

    private class UpdateStepDatabase extends AsyncTask<Void, Void, List<String>> {
        @Override
        protected List<String> doInBackground(Void... params) {
            Step step = null;
            User user = null;
            List<String> info = new ArrayList<>();
            String[] details = textInputEditEachstep.getText().toString().split(" ");
            if (details.length == 2) {
                int sid = Integer.parseInt(details[0]);
                try {
                    if (db1.stepDao().findByID(sid).getUid() == id) {
                        step = db1.stepDao().findByID(sid);
                        step.setSteps(Integer.parseInt(details[1]));
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
                        String dateString = sdf.format(new Date());
                        step.setInserttime(dateString);
                    }
                } catch (Exception e) {
                }
            }
            if (step != null) {
                db1.stepDao().updateSteps(step);
                List<Step> steps = db1.stepDao().findByUID(id);
                int total = 0;
                for (Step step1 : steps) {
                    total = total + step1.getSteps();
                }
                user = db.userDao().findByID(id);
                user.setTotalsteps(total);
                db.userDao().updateUsers(user);
                info.add(String.valueOf(user.getTotalsteps()));
                for (Step temp : steps) {
                    if (temp.getUid() == id) {
                        Step step2 = new Step();
                        step2 = temp;
                        info.add("id: " + step2.getStepid() + ", steps: " + step2.getSteps() + ", time: " + step2.getInserttime() + "\n");
                    }
                }
            }
            return info;
        }

        @Override
        protected void onPostExecute(List<String> details) {
            if (!details.isEmpty()) {
                textInputLayoutTotalstep.setHint(details.get(0));
                textViewTotalstep.setText("The steps have changed to " + details.get(0));
                String info = "All info: ";
                if (details.get(1)!=null){
                for (int i = 1; i < details.size(); i++) {
                    info = info + details.get(i);
                }}
                textView_eachstep.setText(info);
            }
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false);
            }
            ft.detach(new StepFragment()).attach(new StepFragment()).commit();
        }
    }

}
