package com.example.xukaijun.login.sql;

import android.util.Log;

import com.example.xukaijun.login.model.Consumption;
import com.example.xukaijun.login.model.Credential;
import com.example.xukaijun.login.model.Cuser;
import com.example.xukaijun.login.model.Food;
import com.example.xukaijun.login.model.Report;
import com.example.xukaijun.login.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
//import net.sf.json.JSONArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class RestClient {
    private static final String BASE_URL = "http://118.138.79.93:8080/Calorie/webresources/";

    public static List<Cuser> findAllCuser() {
        final String methodPath = "calories.cuser/"; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        List<Cuser> cuserList = new ArrayList<>();
        String textResult = "";
        JSONArray jsonArray;
        JsonObject cuserjson = new JsonObject();
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            jsonArray = new JSONArray(textResult);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject job = jsonArray.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
                    Cuser cuser = new Cuser();
                    cuser.setUsid(job.getInt("usid"));
                    cuser.setStepspermile(job.getLong("stepspermile"));
                    cuser.setUgender(job.getString("ugender"));
                    cuser.setLevelofactivity(job.getInt("levelofactivity"));
                    cuser.setUheight(job.getDouble("uheight"));
                    String[] strs = job.getString("dob").split("'");
                    Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(strs[0]);
                    cuser.setDob(date1);
                    cuser.setUsurname(job.getString("usurname"));
                    cuser.setUname(job.getString("uname"));
                    cuser.setUaddress(job.getString("uaddress"));
                    cuser.setUemail(job.getString("uemail"));
                    cuser.setUpostcode(Short.valueOf(job.getString("upostcode")));
                    cuser.setUweight(job.getDouble("uweight"));
                    cuserList.add(cuser);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return cuserList;
    }

    public static List<Food> findAllFood() {
        final String methodPath = "calories.food/"; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        List<Food> foodList = new ArrayList<>();
        String textResult = "";
        JSONArray jsonArray;
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            jsonArray = new JSONArray(textResult);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject job = jsonArray.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
                    Food food = new Food();
                    food.setFid(job.getInt("fid"));
                    food.setSunit(job.getString("sunit"));
                    food.setSamount(job.getInt("samount"));
                    food.setCamount(job.getLong("camount"));
                    food.setCategory(job.getString("category"));
                    food.setFat(job.getLong("fat"));
                    food.setFname(job.getString("fname"));
                    foodList.add(food);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return foodList;
    }

    public static List<Consumption> findAllConsumption() {
        final String methodPath = "calories.consumption/"; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        List<Consumption> consumptionList = new ArrayList<>();
        String textResult = "";
        JSONArray jsonArray;
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            jsonArray = new JSONArray(textResult);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject job = jsonArray.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
                    Consumption consumption = new Consumption();
                    Cuser cuser = new Cuser();
                    Food food = new Food();
                    consumption.setCdate(job.getString("cdate"));
                    consumption.setConsumpid(job.getInt("consumpid"));
                    consumption.setCquantity(job.getDouble("cquantity"));
                    cuser.setUsid(job.getJSONObject("usid").getInt("usid"));
                    cuser.setStepspermile(job.getJSONObject("usid").getLong("stepspermile"));
                    cuser.setUgender(job.getJSONObject("usid").getString("ugender"));
                    cuser.setLevelofactivity(job.getJSONObject("usid").getInt("levelofactivity"));
                    cuser.setUheight(job.getJSONObject("usid").getDouble("uheight"));
                    String[] strs1 = job.getJSONObject("usid").getString("dob").split("'");
                    Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(strs1[0]);
                    cuser.setDob(date2);
                    cuser.setUsurname(job.getJSONObject("usid").getString("usurname"));
                    cuser.setUname(job.getJSONObject("usid").getString("uname"));
                    cuser.setUaddress(job.getJSONObject("usid").getString("uaddress"));
                    cuser.setUemail(job.getJSONObject("usid").getString("uemail"));
                    cuser.setUpostcode(Short.valueOf(job.getJSONObject("usid").getString("upostcode")));
                    cuser.setUweight(job.getJSONObject("usid").getDouble("uweight"));
                    consumption.setUsid(cuser);
                    food.setFid(job.getJSONObject("fid").getInt("fid"));
                    food.setSunit(job.getJSONObject("fid").getString("sunit"));
                    food.setSamount(job.getJSONObject("fid").getInt("samount"));
                    food.setCamount(job.getJSONObject("fid").getLong("camount"));
                    food.setCategory(job.getJSONObject("fid").getString("category"));
                    food.setFat(job.getJSONObject("fid").getLong("fat"));
                    food.setFname(job.getJSONObject("fid").getString("fname"));
                    consumption.setFid(food);
                    consumptionList.add(consumption);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return consumptionList;
    }

    public static List<Report> findAllReport() {
        final String methodPath = "calories.report/"; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        List<Report> reports = new ArrayList<>();
        String textResult = "";
        JSONArray jsonArray;
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            jsonArray = new JSONArray(textResult);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject job = jsonArray.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
                    Report report =new Report();
                    Cuser cuser = new Cuser();
                    String[] strs1 = job.getString("cdate").split("'");
                    report.setCdate(strs1[0]);
                    report.setRgoals(job.getInt("rgoals"));
                    report.setRid(job.getInt("rid"));
                    report.setTotalburned(job.getDouble("totalburned"));
                    report.setTotalconsumed(job.getDouble("totalconsumed"));
                    report.setTotalsteps(job.getInt("totalsteps"));
                    cuser.setUsid(job.getJSONObject("usid").getInt("usid"));
                    cuser.setStepspermile(job.getJSONObject("usid").getLong("stepspermile"));
                    cuser.setUgender(job.getJSONObject("usid").getString("ugender"));
                    cuser.setLevelofactivity(job.getJSONObject("usid").getInt("levelofactivity"));
                    cuser.setUheight(job.getJSONObject("usid").getDouble("uheight"));
                    String[] strs2 = job.getJSONObject("usid").getString("dob").split("'");
                    Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(strs2[0]);
                    cuser.setDob(date2);
                    cuser.setUsurname(job.getJSONObject("usid").getString("usurname"));
                    cuser.setUname(job.getJSONObject("usid").getString("uname"));
                    cuser.setUaddress(job.getJSONObject("usid").getString("uaddress"));
                    cuser.setUemail(job.getJSONObject("usid").getString("uemail"));
                    cuser.setUpostcode(Short.valueOf(job.getJSONObject("usid").getString("upostcode")));
                    cuser.setUweight(job.getJSONObject("usid").getDouble("uweight"));
                    report.setUsid(cuser);
                    reports.add(report);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return reports;
    }

    public static List<String> findAllCategory() {
        final String methodPath = "calories.food/"; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        List<String> foodList = new ArrayList<String>();
        String textResult = "";
        JSONArray jsonArray;
        JsonObject cuserjson = new JsonObject();
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            jsonArray = new JSONArray(textResult);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++){
                    try {
                        JSONObject foodJson = jsonArray.getJSONObject(i);
                        String category = foodJson.get("category").toString();
                        if(!foodList.contains(category)){
                            foodList.add(category);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return foodList;
    }

    public static String credentialfindByCuser(int usid) {
        final String methodPath = "calories.credential/findByUsid/" + usid; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return textResult;
    }

    public static Credential credentialfindByUsername(String username) {
        final String methodPath = "calories.credential/" + username; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonOb = new JSONObject();
        Credential credential = new Credential();
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            JSONObject job = new JSONObject(textResult);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
            credential.setCusername(job.getString("cusername"));
            credential.setCpassword(job.getString("cpassword"));
            String[] strs = job.getString("sdate").split("'");
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(strs[0]);
            credential.setSdate(date1);
            Cuser cuser = new Cuser();
            cuser.setUsid(job.getJSONObject("usid").getInt("usid"));
            cuser.setStepspermile(job.getJSONObject("usid").getLong("stepspermile"));
            cuser.setUgender(job.getJSONObject("usid").getString("ugender"));
            cuser.setLevelofactivity(job.getJSONObject("usid").getInt("levelofactivity"));
            cuser.setUheight(job.getJSONObject("usid").getDouble("uheight"));
            String[] strs1 = job.getJSONObject("usid").getString("dob").split("'");
            Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(strs1[0]);
            cuser.setDob(date2);
            cuser.setUsurname(job.getJSONObject("usid").getString("usurname"));
            cuser.setUname(job.getJSONObject("usid").getString("uname"));
            cuser.setUaddress(job.getJSONObject("usid").getString("uaddress"));
            cuser.setUemail(job.getJSONObject("usid").getString("uemail"));
            cuser.setUpostcode(Short.valueOf(job.getJSONObject("usid").getString("upostcode")));
            cuser.setUweight(job.getJSONObject("usid").getDouble("uweight"));
            credential.setUsid(cuser);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return credential;
    }

    public static Cuser UserfindByid(int id) {
        final String methodPath = "calories.cuser/" + id; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonOb = new JSONObject();
        Cuser cuser = new Cuser();
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            JSONObject job = new JSONObject(textResult);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
            cuser.setUsid(job.getInt("usid"));
            cuser.setStepspermile(job.getLong("stepspermile"));
            cuser.setUgender(job.getString("ugender"));
            cuser.setLevelofactivity(job.getInt("levelofactivity"));
            cuser.setUheight(job.getDouble("uheight"));
            String[] strs1 = job.getString("dob").split("'");
            Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(strs1[0]);
            cuser.setDob(date2);
            cuser.setUsurname(job.getString("usurname"));
            cuser.setUname(job.getString("uname"));
            cuser.setUaddress(job.getString("uaddress"));
            cuser.setUemail(job.getString("uemail"));
            cuser.setUpostcode(Short.valueOf(job.getString("upostcode")));
            cuser.setUweight(job.getDouble("uweight"));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return cuser;
    }

    public static Food foodfindByname(String fname) {
        final String methodPath = "calories.food/findByFname/" + fname; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        List<Food> foodList = new ArrayList<>();
        JSONArray jsonArray;
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            jsonArray = new JSONArray(textResult);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject job = jsonArray.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
                    Food food = new Food();
                    food.setFid(job.getInt("fid"));
                    food.setSunit(job.getString("sunit"));
                    food.setSamount(job.getInt("samount"));
                    food.setCamount(job.getLong("camount"));
                    food.setCategory(job.getString("category"));
                    food.setFat(job.getLong("fat"));
                    food.setFname(job.getString("fname"));
                    foodList.add(food);
                }
                return foodList.get(0);}
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return null;
    }


    public static List<String> FindByCategory(String category) {
        final String methodPath = "calories.food/findByCategory/" + category; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        JSONArray jsonArray;
        List<String> foodList = new ArrayList<String>();
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            jsonArray = new JSONArray(textResult);
            for (int i = 0; i < jsonArray.length(); i++){
                try {
                    JSONObject foodJson = jsonArray.getJSONObject(i);
                    String foodName = foodJson.get("fname").toString();
                    if(!foodList.contains(foodName)){
                        foodList.add(foodName);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return foodList;
    }

    public static String cuserfindBySurnameandname(String usurname, String uname) {
        final String methodPath = "calories.cuser/findBySurnameandname/" + usurname + "/" + uname; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return textResult;
    }

    public static String findReportByUidandDate(int id, String date) {
        final String methodPath = "calories.report/caloriesReport/" + id + "/" + date; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        String info = " ";
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return textResult;
    }

    public static String findConsumptionByUidandDate(int id, String date) {
        final String methodPath = "calories.consumption/tConsumed/" + id + "/" + date; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        String info = " ";
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            JSONObject job = new JSONObject(textResult);
            info = job.getString("total consumed calories");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return info;
    }

    public static Double findtCalories(int id) {
        final String methodPath = "calories.cuser/tCalories/" + id ; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        Double info = 0.0;
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            JSONObject job = new JSONObject(textResult);
            info = job.getDouble("total calories");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return info;
    }

    public static Double findcaloriePerStep(int id) {
        final String methodPath = "calories.cuser/caloriePerStep/" + id ; //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        Double info = 0.0;
//Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to GET
            conn.setRequestMethod("GET");
//add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
            JSONObject job = new JSONObject(textResult);
            info = job.getDouble("calorie per step");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        return info;
    }

    public static void deleteFood(int fid) {
        final String methodPath = "calories.food/" + fid;
        URL url = null;
        HttpURLConnection conn = null;
        String txtResult = "";
// Making HTTP request
        try {
            url = new URL(BASE_URL + methodPath); //open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the connection method to GET
            conn.setRequestMethod("DELETE");
            Log.i("error", new Integer(conn.getResponseCode()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
    }



    public static void createCuser(Cuser cuser) { //initialise
        URL url = null;
        HttpURLConnection conn = null;
        final String methodPath = "calories.cuser/";
        try {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").create();
            String jsonUser = gson.toJson(cuser);
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to POST
            conn.setRequestMethod("POST"); //set the output to true
            conn.setDoOutput(true);
//set length of the data you want to send
            conn.setFixedLengthStreamingMode(jsonUser.getBytes().length); //add HTTP headers
            conn.setRequestProperty("Content-Type", "application/json");
//Send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(jsonUser);
            out.close();
            Log.i("error", new Integer(conn.getResponseCode()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
    }

    public static void createReport(Report report) { //initialise
        URL url = null;
        HttpURLConnection conn = null;
        final String methodPath = "calories.report/";
        try {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").create();
            String jsonUser = gson.toJson(report);
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to POST
            conn.setRequestMethod("POST"); //set the output to true
            conn.setDoOutput(true);
//set length of the data you want to send
            conn.setFixedLengthStreamingMode(jsonUser.getBytes().length); //add HTTP headers
            conn.setRequestProperty("Content-Type", "application/json");
//Send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(jsonUser);
            out.close();
            Log.i("error", new Integer(conn.getResponseCode()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
    }

    public static void createCredential(Credential credential) { //initialise
        URL url = null;
        HttpURLConnection conn = null;
        final String methodPath = "calories.credential/";
        try {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").create();
            String jsonCredential = gson.toJson(credential);
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to POST
            conn.setRequestMethod("POST"); //set the output to true
            conn.setDoOutput(true);
//set length of the data you want to send
            conn.setFixedLengthStreamingMode(jsonCredential.getBytes().length); //add HTTP headers
            conn.setRequestProperty("Content-Type", "application/json");
//Send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(jsonCredential);
            out.close();
            Log.i("error", new Integer(conn.getResponseCode()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
    }

    public static void createConsumption(Consumption consumption) { //initialise
        URL url = null;
        HttpURLConnection conn = null;
        final String methodPath = "calories.consumption/";
        try {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").create();
            String jsonCredential = gson.toJson(consumption);
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to POST
            conn.setRequestMethod("POST"); //set the output to true
            conn.setDoOutput(true);
//set length of the data you want to send
            conn.setFixedLengthStreamingMode(jsonCredential.getBytes().length); //add HTTP headers
            conn.setRequestProperty("Content-Type", "application/json");
//Send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(jsonCredential);
            out.close();
            Log.i("error", new Integer(conn.getResponseCode()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
    }

    public static void createFood(Food food) { //initialise
        URL url = null;
        HttpURLConnection conn = null;
        final String methodPath = "calories.food/";
        try {
            Gson gson = new Gson();
            String stringFoodJson = gson.toJson(food);
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to POST
            conn.setRequestMethod("POST"); //set the output to true
            conn.setDoOutput(true);
//set length of the data you want to send
            conn.setFixedLengthStreamingMode(stringFoodJson.getBytes().length); //add HTTP headers
            conn.setRequestProperty("Content-Type", "application/json");
//Send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(stringFoodJson);
            out.close();
            Log.i("error", new Integer(conn.getResponseCode()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
    }

    public static void updateFood(Food food) { //initialise
        URL url = null;
        HttpURLConnection conn = null;
        final String methodPath = "calories.food/" + food.getFid();
        try {
            Gson gson = new Gson();
            String stringFoodJson = gson.toJson(food);
            url = new URL(BASE_URL + methodPath);
//open the connection
            conn = (HttpURLConnection) url.openConnection();
//set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
//set the connection method to POST
            conn.setRequestMethod("PUT"); //set the output to true
            conn.setDoOutput(true);
//set length of the data you want to send
            conn.setFixedLengthStreamingMode(stringFoodJson.getBytes().length); //add HTTP headers
            conn.setRequestProperty("Content-Type", "application/json");
//Send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(stringFoodJson);
            out.close();
            Log.i("error", new Integer(conn.getResponseCode()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
    }
}
